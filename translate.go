// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Package vonny transforms text by making it corrupted yet still recognizable.
//
// Translate will transform single piece of text.
// If more parameter tuning needed, New returns instance of Vonny with custom settings applied
//
// Translate performs NFC transformation to make sure that characters are in same normal form.
package vonny

import (
	"golang.org/x/text/language"
)

// Translate performs transformation with default settings.
// It will cache created instances of vonny, so no additional overhead is expected.
func Translate(tag language.Tag, s string) string {
	v := vonnies[tag]
	if v == nil {
		v = New(VonnyConfig{Language: tag})
		vonnies[tag] = v
	}
	return v.Translate(s)
}

var vonnies = make(map[language.Tag]*Vonny)
