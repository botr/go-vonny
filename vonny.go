// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package vonny

import (
	"bytes"
	"math"
	"math/rand"
	"unicode"
	"unicode/utf8"

	"golang.org/x/text/language"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"pkg.botr.me/monogram"
)

// VonnyConfig allows to tune transform parameters
type VonnyConfig struct {
	VowelExcludeRate float64 // control exclusion of vowels (default: 61.1)
	ExcludeRate      float64 // control exclusion of any character (default: 50.0)
	SwapRate         float64 // how often to swap characters instead of delete (default: 50.0)
	Strength         float64 // rate of changes (default: 50.0)

	Language language.Tag
}

// Vonny corrupts everything you feed to it.
// Implements transform.Transformer interface so it can be used in transform.Chain.
type Vonny struct {
	swaprate float64
	strength float64

	weights monogram.RuneWeights
	cpw     float64
	wcount  int
}

// New returns customized instance of Vonny. Configuration parameters described in VonnyConfig
func New(config VonnyConfig) *Vonny {
	vonny := Vonny{
		swaprate: parameters.swaprate.norm(config.SwapRate),
		strength: parameters.strength.norm(config.Strength),
		weights:  monogram.Weights(config.Language),
		cpw:      monogram.CPW(config.Language),
	}

	if vonny.cpw < 1 {
		vonny.cpw = 4 // some decent number to avoid division by zero
	}

	e := parameters.exclude.norm(config.ExcludeRate)
	ve := parameters.vowelExclude.norm(config.VowelExcludeRate)

	// precalc probabilities
	for r, f := range vonny.weights {
		if monogram.IsVowel(config.Language, r) {
			f = f * ve
		}
		vonny.weights[r] = math.Pow(f, e)
	}
	return &vonny
}

// Reset does nothing and exists only to correctly implement transform.Transformer interface
func (*Vonny) Reset() {}

// Transform method applies Vonny transformation to source text
func (vonny *Vonny) Transform(dst, src []byte, atEOF bool) (nDst, nSrc int, err error) {
	tmp := vonny.transform(src)
	if len(tmp) > len(dst) {
		err = transform.ErrShortDst
		return
	}

	nDst = copy(dst, tmp)
	nSrc = len(src)
	return
}

// Translate transforms text into corrupted version of itself. Uses NFC normalization beforehand.
func (vonny *Vonny) Translate(s string) string {
	s, _, _ = transform.String(transform.Chain(norm.NFC, vonny), s)
	return s
}

func (vonny *Vonny) transform(src []byte) (dst []byte) {
	// Allocates more than needed since len(s) > RuneCount.
	// This isn't very precise for non-ASCII code points but RuneCount is too slow.
	indexes := make([]int, 0, 2*len(src)/int(vonny.cpw)+1)

	// first index should always be word
	prev, i := utf8.DecodeRune(src)
	if prev = unicode.ToUpper(prev); vonny.weights[prev] != 0 {
		indexes = append(indexes, 0)
	}

	for i < len(src) {
		cur, w := utf8.DecodeRune(src[i:])
		cur = unicode.ToUpper(cur)

		if (vonny.weights[cur] == 0) != (vonny.weights[prev] == 0) {
			indexes = append(indexes, i)
		}

		i += w
		prev = cur
	}

	if len(indexes) == 0 {
		return src // nothing to translate
	}

	if len(indexes) == 1 {
		return vonny.transformWord(src[indexes[0]:]) // single word
	}

	// last index always end of string
	indexes = append(indexes, len(src))

	var out bytes.Buffer
	out.Grow(len(src))
	out.Write(src[:indexes[0]])

	for i = 0; i+1 < len(indexes); i += 2 {
		start, end := indexes[i], indexes[i+1]
		out.Write(vonny.transformWord(src[start:end]))

		if i+2 < len(indexes) {
			out.Write(src[end:indexes[i+2]]) // leftover characters
		}
	}

	// recalculate avg cpw for this particular corpus
	wc, ls, li := float64(vonny.wcount), float64(len(src)), float64(len(indexes))
	vonny.cpw = (vonny.cpw*wc + ls) / (wc + li/2)

	return out.Bytes()
}

func (vonny *Vonny) transformWord(word []byte) []byte {
	runes := make([]rune, 0, len(word))
	swaps := make([]int, 0, len(word))     // character swaps
	probs := make([]float64, 0, len(word)) // probability of change for character

	mlen := 0

	var prob float64
	for i := 0; i < len(word); mlen++ {
		r, w := utf8.DecodeRune(word[i:])
		i += w

		runes = append(runes, r)
		swaps = append(swaps, mlen)
		probs = append(probs, vonny.weights[unicode.ToUpper(r)])

		prob += probs[mlen]
	}

	if mlen < 2 {
		return word
	}

	probs[0] /= 20
	mlen -= 1
	runes = append(runes, 0)

	nch := vonny.strength * float64(len(runes)) / 2
	nch = math.Floor(rand.Float64()*nch + nch + 1)

	for ; nch > 0 && prob > 0; nch -= 1 {
		var i int
		for j := rand.Float64() * prob; i <= mlen; i++ {
			if j -= probs[i]; j <= 0 {
				break
			}
			if i == mlen {
				break
			}
		}

		j := swaps[i]
		prob -= probs[i]

		probs[i], swaps[i] = probs[mlen], swaps[mlen]
		probs, swaps = probs[:mlen], swaps[:mlen]
		mlen -= 1

		if rand.Float64() < vonny.swaprate {
			runes[j], runes[j+1] = runes[j+1], runes[j]
		} else {
			runes[j] = 0
		}
	}

	var out bytes.Buffer
	out.Grow(len(word))

	for _, r := range runes {
		if r > 0 {
			out.WriteRune(r)
		}
	}
	return out.Bytes()
}

// pr describes float parameter range
type pr struct {
	min, max float64
	def      float64
}

func (p pr) contains(v float64) bool {
	return v >= p.min && v <= p.max
}

func (p pr) norm(v float64) float64 {
	if v == 0 || !parameters.in.contains(v) {
		return p.def
	}

	min, max := parameters.in.min, parameters.in.max
	return p.min + (v-min)*(p.max-p.min)/(max-min)
}

var parameters = struct {
	in pr

	exclude      pr
	vowelExclude pr
	swaprate     pr
	strength     pr
}{
	pr{0.0, 100.0, 0.00},
	pr{0.0, 1.00, 0.50}, // exclude
	pr{1.0, 10.0, 5.50}, // vowelExclude
	pr{0.0, 0.50, 0.25}, // swaprate
	pr{0.0, 0.50, 0.25}, // strength
}
