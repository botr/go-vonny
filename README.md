# go-vonny

This library can be used to vonnify text. It changes neighbour characters or removes them from words.  
Supports same set of languages as `pkg.botr.me/monogram` ([doc](https://pkg.botr.me/monogram), [src](https://bitbucket.org/botr/monogram)).  
If you want to support yours, please send character frequencies for alphabet of choice there.

Intensity of changes can be configured.
Currently vonny only able to make changes for provided locale. For example, when you pass `language.Russian` it won't change latin characters.  
Non-text characters like punctuation symbols, whitespaces or emoji aren't processed or stripped out from input text.

Be aware that input text being converted to NFC unicode form.


## Usage

```shell
$ go get pkg.botr.me/go-vonny@latest
```

```go
package main

import (
	"log"

	"pkg.botr.me/go-vonny"
	"golang.org/x/text/language"
)

func main() {
	str := "Hello World!"
	fmt.Println(str)

	out := vonny.Translate(language.English, str)
	fmt.Println(out) // "Heol Worl!"
}

```


## Etymology of the name

Package vonny named after fictional character "Vonny-the-Pyh" whose prototype is Pooh, the plush bear.  
He speaks using intentionally corrupted words and represents stereotypical low-life person.


## License

```
Copyright (c) 2021 gudvinr

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
```
