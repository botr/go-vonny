// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package vonny

import (
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"golang.org/x/text/language"
)

func TestLanguageT2(t *testing.T) {
	assert := assert.New(t)

	t.Run("English", func(t *testing.T) {
		vonny := New(VonnyConfig{Language: language.English})

		src := "Lorem ipsum dolor sit amet"
		dst := vonny.Translate(src)

		assert.NotEqual(src, dst)
		t.Logf("vonny: %s", dst)
	})

	t.Run("Russian", func(t *testing.T) {
		vonny := New(VonnyConfig{Language: language.Russian})

		// src := "Вонни и Потачок — жизнеутверждающие комиксы"
		src := "привет медвет, я пришёл на obed."
		dst := vonny.Translate(src)

		assert.NotEqual(src, dst)
		assert.Equal(src[len(src)-6:], dst[len(dst)-6:])
		t.Logf("vonny: %s", dst)
	})
}

func TestTextT2(t *testing.T) {
	assert := assert.New(t)

	t.Run("ZeroRunes", func(t *testing.T) {
		vonny := New(VonnyConfig{})

		src := ""
		dst := vonny.Translate(src)

		assert.Equal(src, dst)
	})
}

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())
	os.Exit(m.Run())
}
