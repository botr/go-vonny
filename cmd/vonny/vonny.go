// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"fmt"
	"os"
	"strings"

	"golang.org/x/text/language"
	"pkg.botr.me/go-vonny"
)

// TODO: support reading from STDIN

func main() {
	if len(os.Args) < 3 {
		fmt.Println("not enough arguments")
	}

	lang := language.MustParse(os.Args[1])
	text := strings.Join(os.Args[2:], " ")

	text = vonny.Translate(lang, text)
	fmt.Println(text)
}
