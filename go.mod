module pkg.botr.me/go-vonny

go 1.15

require (
	github.com/stretchr/testify v1.7.0
	golang.org/x/text v0.3.5
	pkg.botr.me/monogram v1.1.1
)
